(function(){
  'use strict';
  var os = require("os");
  var disk = require("diskusage");
  var app = require('express')();
  var http = require('http').Server(app);
  var io = require('socket.io')(http);

  const gb = 1073741824;
  const ram = os.totalmem()/gb;
  const paths = new Set(['/','/home']);

  var used = 0;

  var system = {
    info:{
      arch:os.arch(),
      host:{
        name:os.hostname(),
        home:os.homedir()
      },
      platform:os.platform(),
      release:os.release()
    },
    disk:new Object(),
    memory:new Object(),
    network:new Object()
  }

  function percentageOf(total,per){
    return (per*100)/total;
  }
  function freeMem(){
    used = os.freemem()/gb;
  }

  /*
  ** Updates
  */
  var update = {
    memory(){
      freeMem();
      system.memory={
        size:Number((ram).toFixed(2)),
        free:Number((used).toFixed(2)),
        used:Number((ram-used).toFixed(2)),
        percentage:{
          free:percentageOf(ram,used),
          used:percentageOf(ram,ram-used)
        }
      };
      return system.memory;
    },
    disk(){
      for(let path of paths){
        disk.check(path,(err,info)=>{
          if(err){
            console.error(err);
          }
          else{
            system.disk[path]={
              size:Number((info.total/gb).toFixed(2)),
              free:Number((info.free/gb).toFixed(2)),
              used:Number(((info.total-info.free)/gb).toFixed(2)),
              percentage:{
                free:percentageOf(info.total,info.free),
                used:percentageOf(info.total,info.total-info.free)
              }
            };
          }
        })
      }
      return system.disk;
    },
    network(){
      system.network=os.networkInterfaces();
      return system.network;
    },
    all(){
      this.memory();
      this.disk();
      this.network();
    }
  }
  function sendSystemInfo(){
    return system;
  }

  app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
  });

  io.on('connection', function(socket){
    console.log('Nueva Conexión '+Date.now());
    update.all();
    setTimeout(function () {
      setInterval(function () {
        socket.emit('memoryUpdate',update.memory());
      },998);
      setInterval(function () {
        socket.emit('diskUpdate',update.disk())
      },4998);
      socket.emit('sysInfo',sendSystemInfo())
    }, 1990);
  });

  http.listen(3000, function(){
    console.log('listening on *:3000');
  });
})();
